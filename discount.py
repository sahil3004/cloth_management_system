from database import DatabaseConnection
from product import ProductManagement
from category_manage import CategoryManagement
from util import Utils
import psycopg2
from datetime import datetime 


class Discount:

	def __init__(self, db_connection):
		self.db_connection = db_connection


	def discount_menu(self):
		while True:
			print("\n\n-------------------------")
			print("💸 Discount Management:")
			print("-------------------------")
			print("1. Add Product Discount")
			print("2. Delete Discount")
			print("3. Listing Product Discount ")
			print("4. Back to Previous Menu")

			choice = input("\nPlease select an option (1-3): ")
			if choice not in ['1', '2', '3', '4']:
				print("Invalid input, please choose the right option.")
				continue

			if choice == '1':
				self.add_product_discount()
			elif choice == '2':
				self.discount_delete()
			elif choice == '3':
				self.list_discount()
			elif choice == '4':
				print("Returing to Admin menu ...")
				break
				

	def add_product_discount(self):
		product = ProductManagement(self.db_connection)
		products = product.list_product()

		if not products:
			print("\nNo products found. Please add products first..")
			return
		
		while True:
			try:	
				print("\nProduct Discount .....")
				product_id = int(input("Enter the Product ID : "))

				product_exists =  any(product_id == product[0] for product in products)

				if product_exists:
					discount_exists = self.check_product_discount_exists(product_id)

					if discount_exists:
						update_option = input("A discount already exists for this product. Do you want to update it? (yes/no): ").lower()
						if update_option == 'yes':
							existing_discount = self.get_product_discount(product_id)
							print("\nExisting discount details : ")
							print("\n-----------------------------")
							print("Coupon Code:", existing_discount['coupon_code'])
							print("Discount Amount:", existing_discount['Discount Amount'])
							print("Start Date:", existing_discount['Start-Date'])
							print("End Date:", existing_discount['End Date'])
							print("-----------------------------")
							print()

							coupon_code = input("Enter the Coupon Code : ")
							discount_amount = int(input("Enter the Discount Amount :"))
							start_date = input("Enter the Start Date (YYYY-MM-DD): ")
							end_date = input("Enter the end Date (YYYY-MM-DD): ")

							valid_date = Utils(self.db_connection)
							valid_date.is_valid_date(start_date)

							check_valid_date = valid_date.is_valid_start_end_date(start_date,end_date)

							if not valid_date.is_valid_date(start_date) or not valid_date.is_valid_date(end_date):
								print("Invalid date format. Please use YYYY-MM-DD.")
								continue

							start_time = datetime.strptime(start_date, "%Y-%m-%d")
							end_time = datetime.strptime(end_date, "%Y-%m-%d")

							if valid_date.is_valid_start_end_date(start_time,end_time):
								self.update_product_discount(product_id, coupon_code, discount_amount, start_date, end_date)
								print("\nProduct discount updated successfully.")
								break
							else:
								print("Invalid date range. Start date must be before end date.")

					else:
						coupon_code = input("\nEnter the Coupon Code : ")
						discount_amount = int(input("Enter the Discount Amount :"))
						start_date = input("Enter the Start Date (YYYY-MM-DD): ")
						end_date = input("Enter the end Date (YYYY-MM-DD): ")

						valid_date = Utils(self.db_connection)
						valid_date.is_valid_date(start_date)

						check_valid_date = valid_date.is_valid_start_end_date(start_date,end_date)

						if not valid_date.is_valid_date(start_date) or not valid_date.is_valid_date(end_date):
							print("Invalid date format. Please use YYYY-MM-DD.")
							continue

						start_time = datetime.strptime(start_date, "%Y-%m-%d")
						end_time = datetime.strptime(end_date, "%Y-%m-%d")

						if valid_date.is_valid_start_end_date(start_time,end_time):
							break
						else:
							print("Invalid date range. Start date must be before end date.")
					# else:
					# 	print("No Discounts Exists")
				else:
					print(" product Not exists. ")
			except ValueError:
				print("Invalid input. Please enter valid data.")

		try:
			insert_query = "INSERT INTO discounts (product_id,coupon_code,discount_amount,start_date,end_date) VALUES (%s,%s,%s,%s,%s)"
			self.db_connection.cur.execute(insert_query,(product_id,coupon_code,discount_amount,start_date,end_date))
			self.db_connection.conn.commit()
			print(f"\nDiscount Added Sucessfully...\nCoupan Code : {coupon_code}\nDiscount Amount : {discount_amount}\nDate : {start_date} to {end_date}")
		except Exception as error:
			print("Error adding product discount",error)


	def check_product_discount_exists(self,product_id):

		query = "SELECT COUNT(*) FROM discounts WHERE product_id = %s"
		self.db_connection.cur.execute(query,(product_id,))
		result = self.db_connection.cur.fetchone()
		return result[0] > 0


	def update_product_discount(self,product_id,coupon_code,discount_amount,start_date,end_date):
		try:
			update_query = "UPDATE discounts SET coupon_code = %s, discount_amount = %s, start_date = %s, end_date = %s WHERE product_id = %s"
			self.db_connection.cur.execute(update_query,(coupon_code,discount_amount,start_date,end_date,product_id))
			self.db_connection.conn.commit()
			print(f"\nDiscount Update Sucessfully...\nCoupan Code : {coupon_code}\nDiscount Amount : {discount_amount}\nStart Date : {start_date}\nEnd Date : {end_date}")
		except Exception as error:
			print("Error Updating Discount..",error)


	def get_product_discount(self,product_id):
		try:
			query = "SELECT coupon_code, discount_amount, start_date, end_date FROM discounts WHERE product_id = %s"
			self.db_connection.cur.execute(query,(product_id,))
			discount = self.db_connection.cur.fetchone()
			return {
				"coupon_code" : discount[0],
				"Discount Amount" : discount[1],
				"Start-Date" : discount[2],
				"End Date" : discount[3] }
		except Exception as error:
			print("Error Getting the Discount",error)


	def list_discount(self):
		try:
			query = """SELECT discounts.discount_id, products.name AS product_name, discounts.coupon_code, discounts.discount_amount,
						discounts.start_date,discounts.end_date FROM discounts
						INNER JOIN products ON discounts.product_id = products.product_id"""

			self.db_connection.cur.execute(query)
			discounts = self.db_connection.cur.fetchall()

			if discounts:
				print("\nAvilable Discounts..\n")
				for discount in discounts:
					discount_id, product_name, coupon_code, discount_amount, start_date, end_date = discount
					print(f"Discount ID : {discount_id}\nProduct Name : {product_name}\nCoupon Code : {coupon_code}\nDiscount Amount : {discount_amount}\nStart-Date : {start_date}\nEnd-Date : {end_date}")
					print()
				return discounts
			else:
				print("\nNo discounts Found")

		except Exception as error:
			print("ERROR Listing Discounts",error)
			return[]


	def discount_delete(self):
		while True:
			self.list_discount()

			discount_id = input("Enter the Discount ID to Delete ('exit' For Returing) : ")
			if discount_id.lower() == 'exit':
				print("\nReturning To Discount Menu....\n")
				break

			if discount_id.isdigit():
				discount_id = int(discount_id)

				delete_discount = Utils(self.db_connection)
				delete_discount.delete_item('discounts',['discount_id',discount_id])
			else:
				print("Please Enter Valid Input...")
