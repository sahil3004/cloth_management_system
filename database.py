import psycopg2

class DatabaseConnection:

	def __init__(self,hostname,database,username,pwd,port_id):
		self.hostname = hostname
		self.database = database
		self.username = username
		self.pwd = pwd
		self.port_id = port_id
		self.conn = None
		self.cur = None


	def connect(self):
		try:
			self.conn = psycopg2.connect(
				host = self.hostname,
				dbname = self.database,
				user = self.username,
				password = self.pwd,
				port = self.port_id
				)

			self.cur = self.conn.cursor()
		except Exception as error:
			print(error)


	def close_connection(self):
		if self.cur is not None:
			self.cur.close()
		if self.conn is not None:
			self.conn.close()



