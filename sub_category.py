import psycopg2
from psycopg2 import sql
from database import DatabaseConnection
from util import Utils
from category_manage import CategoryManagement

class SubCategory:

	def __init__(self,db_connection):
		self.db_connection = db_connection


	def sub_category_menu(self):
		while True:
			print("\n-----------------------------")
			print("📂Sub-Category Management...")
			print("-----------------------------")
			print("1. Add Sub-Category")
			print("2. Update Sub-Category")
			print("3. Delete Sub-Category")
			print("4. List SubCategories")
			print("5. Back To Admin Menu")
			print()

			choice = input("Please select an options (1-5): ")
			if choice not in ['1','2','3','4','5']:
				print("Invalid input, Please select an right option.")
				continue

			if choice == '1':
				self.add_sub_category()
			elif choice == '2':
				self.update_sub_category()
			elif choice == '3':
				self.delete_sub_category()
			elif choice == '4':
				self.list_sub_category()
			elif choice == '5':
				print("Returing the Admin menu....")
				break


	def add_sub_category(self):
		category_manage = CategoryManagement(self.db_connection)
		categories = category_manage.list_category()	

		if not categories:
			print("No Category Found. Please add categories first.")
			return

		while True:
			try:
				category_id = int(input("Enter the Category ID : "))
			
				if any(category_id == category[0] for category in categories):
					break
				else:
					print("Invalid Category ID. Please select valid from the List.")
	
			except ValueError:
				print("Invalid input, Please enter valid category Id.")

		print("category_id", category_id)
		self.list_sub_category(category_id=category_id)
		while True:
			sub_category_name = input("Enter the Sub Category Name : ").strip()
			if not sub_category_name:
				print("Please enter the Sub-Category Name...")
				continue

			try:
				query = " INSERT INTO sub_category(name, category_id) VALUES (%s, %s)"
				self.db_connection.cur.execute(query,(sub_category_name,category_id))
				self.db_connection.conn.commit()
				print(f'\nSub-Category {sub_category_name} Added Sucessfully.')
				break
			except Exception as error:
				print("Error Adding Sub Category", error)
		

	def list_sub_category(self,show=True,category_id=None):
		try:
			if category_id:
				query = "SELECT sub_category_id, name FROM sub_category WHERE category_id = %s"
				self.db_connection.cur.execute(query,(category_id,))

			else:
				query = "SELECT sub_category_id, name FROM sub_category"
				self.db_connection.cur.execute(query)
			categories = self.db_connection.cur.fetchall()

			if categories:
				print("\nAvailable Sub Categories....\n")
				if show:
					for index,category in enumerate(categories,start=1):
						sub_category_id,name = category
						print(f"\nSub Category ID : {index}   Name: {name}")
				return categories
			else:
				print("\nSub Categories Not Found..")

		except Exception as error:
			print("ERROR Listing Sub Categories", error)


	def update_sub_category(self):
		while True:
			sub_category =  self.list_sub_category(show = False)
			if not sub_category:
				print("No Sub Categories Avilable..")
				return 

			print("\n----------------------------")
			print("Available Sub Categories...")
			print("----------------------------")

			for index,subcategories in enumerate(sub_category,start = 1):
				sub_category_id,name = subcategories
				print(f"{index} :   {name}")

			sub_category_index = input("\nEnter the Sub-Category Option to update ('exit' for Category Menu ) : ")
			if sub_category_index.lower() == 'exit':
				print("Back to manin menu...")
				break

			if sub_category_index.isdigit():
				sub_category_index = int(sub_category_index)

				if  1 <= sub_category_index <= len(sub_category):
					sub_category_id, name = sub_category[sub_category_index-1]
					
					while True:
						new_category_name = input("Enter the New Category Name : ").strip()
						if not new_category_name:
							print("Invalid input. Please Enter the Sub-Category Name...")
							continue

						try:
							query = "UPDATE sub_category SET name = %s WHERE sub_category_id = %s"
							self.db_connection.cur.execute(query,(new_category_name,sub_category_id))
							self.db_connection.conn.commit()
							print(f"Sub Category with ID {sub_category_id} updatede Sucessfully.")
							break
						except Exception as error:
							print("Error Updating Category",error)
						break


	def associated_products(self,sub_category_id):
		query = "SELECT COUNT(*) FROM products WHERE sub_category_id = %s"
		self.db_connection.cur.execute(query,(sub_category_id,))
		count = self.db_connection.cur.fetchone()[0]
		return count > 0


	def delete_sub_category(self):
		subcategories = self.list_sub_category()

		while True:
			sub_category_index = input("Please Choose Above Option for Delete Sub-Category ('exit' for back menu ) : "  )
			if sub_category_index.lower() == 'exit':
				print("Returning to Menu.....")
				break
			
			if sub_category_index.isdigit():
				sub_category_index = int(sub_category_index)

				if 1 <= sub_category_index <= len(subcategories):
					subcategory_id = subcategories[sub_category_index - 1][0]

					associated_products = self.associated_products(subcategory_id)
					if associated_products:
						print("There are associated products with this sub-category. You must remove or delete them first.")
					else:
						delete_sub_category_obj = Utils(self.db_connection)
						delete_sub_category_obj.delete_item('sub_category',['sub_category_id',subcategory_id])
						subcategories = self.list_sub_category()
				else:
					print("Invalid Subcategory Index. Please select a valid index from the list.")
			else:
				print("Please enter a valid input.")