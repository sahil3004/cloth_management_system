from database import DatabaseConnection
import psycopg2

class DataInserter:

	def __init__(self,db_connection):
		self.db_connection = db_connection


	def insert_user(self):
		data = [
					(1, 'sahil','12345','sahil@gmail.com',True),
					(2, 'mahak','96247','mahak@gmail.com',False)
				]

		insert_script = ''' INSERT INTO users(user_id, name, password, email, user_type) VALUES (%s,%s,%s,%s,%s) '''

		try:
			self.db_connection.cur.executemany(insert_script,data)
			self.db_connection.conn.commit()
		except Exception as error:
			print("Error inserting user data:",error)


	def insert_size_data(self):
		data = [(1, 'Small'),
				(2, 'Medium'),
				(3, 'Large')]
		
		insert_script = ''' INSERT INTO size(size_id , name) VALUES (%s,%s)'''

		try:
			self.db_connection.cur.executemany(insert_script,data)
			self.db_connection.conn.commit()
		except Exception as error:
	 		print("Error inserting szie data", error)


if __name__ == "__main__":
	hostname = 'localhost'
	database = 'shopping'
	username = 'postgres'
	pwd = 'root'
	port_id = 5432

	db = DatabaseConnection(hostname,database,username,pwd,port_id)
	db.connect()

	data_inserter = DataInserter(db)

	data_inserter.insert_user()
	data_inserter.insert_size_data()

	db.close_connection()




