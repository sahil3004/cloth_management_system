from database import DatabaseConnection
import psycopg2
from product import ProductManagement
from wishlist import Wishlist
from cart import ViewCart

class BrowseClothing:

	def __init__(self,db_connection,user_id):

		self.db_connection = db_connection
		self.user_id = user_id


	def browse_women_clothing(self,user_id):

		try:
			query = """SELECT c.name AS category_name, s.name AS subcategory_name, s.sub_category_id
					   FROM sub_category s
					   INNER JOIN category c ON s.category_id = c.category_id
					   WHERE c.category_id = 2"""
			self.db_connection.cur.execute(query)
			subcategories = self.db_connection.cur.fetchall()

			if not subcategories:
				print("No Women's Clothing subcategories available.")
				return

			print("\nWomen's Clothing Subcategories:\n")
			for index, subcategory in enumerate(subcategories, start=1):
				category_name, subcategory_name, subcategory_id = subcategory
				print(f"{index}: {subcategory_name}")

			subcategory_option = input("\nEnter the Subcategory Option (or 'exit' to go back): ")
			if subcategory_option.lower() == 'exit':
				print("Back to main menu...")
				return

			subcategory_index = int(subcategory_option)
			if 1 <= subcategory_index <= len(subcategories):
				selected_subcategory = subcategories[subcategory_index - 1]
				_, selected_subcategory_name, sub_category_id = selected_subcategory

				query = """SELECT p.product_id, p.name, p.description, p.price, s.name AS size_name, p.stock
						   FROM products p
						   LEFT JOIN size s ON p.size_id = s.size_id
						   WHERE p.sub_category_id = %s"""

				self.db_connection.cur.execute(query, (sub_category_id,))
				products = self.db_connection.cur.fetchall()

				if products:
					print(f"\nProducts in '{selected_subcategory_name}' Subcategory:")
					for index, product in enumerate(products, start=1):
						product_id, name, description,price,size_name,stock= product
						print("------------------------------------------------------------------------------------------")
						print(f"\n{index}")
						print(f"Name: {name}")
						print(f"Description: {description}")
						print(f"Size: {size_name}")
						print(f"Price: INR {price:.2f}")
						print(f"stock : {stock}")
						print("\n------------------------------------------------------------------------------------------")

					product_option = input("\nEnter the Product Option (or 'exit' to go back): ")
					if product_option.lower() == 'exit':
						print("Back to main menu...")
						return

					if product_option.isdigit():
						product_option = int(product_option)

						if 1 <= product_option <= len(products):
							selected_product = products[product_option - 1]


							print(f"Selected Product:")
							print(f"\nProduct ID: {selected_product[0]}")
							print(f"Name: {selected_product[1]}")
							print(f"Description: {selected_product[2]}")
							print(f"Price: INR {selected_product[3]:.2f}")
							print(f"Size: {selected_product[4]}")
							print(f"stock: {selected_product[5]}")

							while True:
								print("\n1. Add to cart")
								print("2. Add to wishlist")
								print("3. Back to menu")

								choice = input("\nSelect the above options (1-3) : ")
								if choice not in ['1','2','3']:
									print("Please select the right option")
									continue

								if choice == '1':
									default_stock = 1

									# Prompt user to choose stock
									user_stock = input(f"Enter stock (default is {default_stock}): ")
									if user_stock.isdigit():
										user_stock = int(user_stock)
										if 1 <= user_stock <= selected_product[5]:  # Check if user stock is within available stock
											default_stock = user_stock

									cart_manage = ViewCart(self.db_connection,user_id)
									cart_manage.add_cart(selected_product[0], user_id,  quntity =default_stock)
									print(f"\n{default_stock} product added into your Cart\n")

								if choice == '2':
									wishlist_obj = Wishlist(self.db_connection,user_id)
									wishlist_obj.add_wishlist(selected_product[0],user_id)
									print(f"\nProduct added into your wishlist")

								if choice == '3':
									print("Returning to Menu...")
									break
				else:
					print(f"\nNo products available in '{selected_subcategory_name}' Subcategory.")
	
			else:
				print("Invalid Subcategory Option. Please enter a valid option.")
		except Exception as e:
			print("Error browsing Women's clothing:", e)


	
	def browse_men_clothing(self,user_id):

		try:
			query = """SELECT c.name AS category_name, s.name AS subcategory_name, s.sub_category_id
						 FROM sub_category s
						 INNER JOIN category c ON s.category_id = c.category_id
						 WHERE c.category_id = 1"""
			self.db_connection.cur.execute(query)
			subcategories = self.db_connection.cur.fetchall()

			if not subcategories:
				print("\nNo Men's Clothing subcategories available.")
				return

			print("\nMen's Clothing Subcategories:\n")
			for index, subcategory in enumerate(subcategories, start=1):
				category_name, subcategory_name, subcategory_id = subcategory
				print(f"{index}: {subcategory_name}")

			subcategory_option = input("\nEnter the Subcategory Option (or 'exit' to go back): ")
			if subcategory_option.lower() == 'exit':
				print("Back to main menu...")
				return

			if subcategory_option.isdigit():
				subcategory_option = int(subcategory_option)

				if 1 <= subcategory_option <= len(subcategories):
					selected_subcategory = subcategories[subcategory_option - 1]
					_, selected_subcategory_name, sub_category_id = selected_subcategory

					query = """SELECT p.product_id, p.name, p.description, p.price,  s.name AS size_name, p.stock, d.coupon_code, d.discount_amount
						   FROM products p
						   LEFT JOIN size s ON p.size_id = s.size_id
						   LEFT JOIN discounts d ON p.product_id = d.product_id
						   WHERE p.sub_category_id = %s"""

					self.db_connection.cur.execute(query, (sub_category_id,))
					products = self.db_connection.cur.fetchall()

					# print(f"products{products}")
					if products:
						print(f"\nProducts in '{selected_subcategory_name}' Subcategory:")
						for index, product in enumerate(products, start=1):
							product_id, name, description,price,size_name,stock,coupon_code,discount_amount = product
							print("------------------------------------------------------------------------------------------")
							print(f"\n{index}")
							print(f"Name: {name}")
							print(f"Description: {description}")
							print(f"Size: {size_name}")
							print(f"Price: INR {price:.2f}")
							print(f"stock : {stock}")

							if coupon_code and discount_amount:
								print(f"\nCoupon code : {coupon_code}")
								print(f"Coupon Discount : {discount_amount}")
							print("\n------------------------------------------------------------------------------------------")

						product_option = input("Enter the Product Option (or 'exit' to go back): ")
						if product_option.lower() == 'exit':
							print("Back to main menu...")
							return

						if product_option.isdigit():
							product_option = int(product_option)

							if 1 <= product_option <= len(products):
								selected_product = products[product_option - 1]

								print(f"\nSelected Product:")
								print(f"\nProduct ID: {selected_product[0]}")
								print(f"Name: {selected_product[1]}")
								print(f"Description: {selected_product[2]}")
								print(f"Price: INR {selected_product[3]:.2f}")
								print(f"Size: {selected_product[4]}")
								print(f"stock: {selected_product[5]}")

								if selected_product[6] and selected_product[7]:
									print(f"\nCoupon code : {selected_product[6]}")
									print(f"Coupon Discount : {selected_product[7]}")

								while True:
									print("\n1. Add to cart")
									print("2. Add to wishlist")
									print("3. Back to menu")

									choice = input("Select the above options (1-3) : ")
									if choice not in ['1','2','3']:
										print("Please select the right option")
										continue

									if choice == '1':
										default_stock = 1

										# Prompt user to choose stock
										user_stock = input(f"\nEnter stock (default is {default_stock}): ")
										if user_stock.isdigit():
											user_stock = int(user_stock)
											if 1 <= user_stock <= selected_product[5]:  # Check if user stock is within available stock
												default_stock = user_stock
											else:
           										 print(f"Invalid stock! Maximum available stock for this product is {selected_product[5]}.")

										cart_manage = ViewCart(self.db_connection,user_id)
										cart_manage.add_cart(selected_product[0], user_id,  quntity =default_stock)
										print(f"\n{default_stock} product added into your Cart")

									if choice == '2':
										wishlist_obj = Wishlist(self.db_connection,user_id)
										wishlist_obj.add_wishlist(selected_product[0],user_id)

									if choice == '3':
										print("Returning to Menu...")
										break

					else:
						print(f"\nNo products available in '{selected_subcategory_name}' Subcategory.")

			else:
				print("Invalid Subcategory Option. Please enter a valid option.")
		except Exception as e:
			print("Error browsing Men's clothing:", e)

