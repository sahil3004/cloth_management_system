import psycopg2
from database import DatabaseConnection
from util import Utils


class CategoryManagement:

	def __init__(self,db_connection):
		self.db_connection = db_connection


	def category_manage(self):
		while True:
			print("--------------------------")
			print("📁Category Management : ")
			print("--------------------------")
			print("\n1. Add Category")
			print("2. Update Category")
			print("3. Delete Category")
			print("4. List Category")
			print("5. Back to Admin Menu")

			choice = input("\nPlease select an Option : ")
			if choice not in ['1','2','3','4','5']:
				print("Invalid input Please Choose valid option.")
				continue

			if choice == '1':
				self.add_category()

			elif choice == '2':
				self.update_category()

			elif choice == '3':
				self.delete_category()

			elif choice == '4':
				self.list_category()

			elif choice == '5':
				print("Returing to Category Menu...")
				break


	def add_category(self):
		self.list_category()
		while True:
			category_name = input("\nEnter the Category Name ('exit' for Exiting...) : ").strip()
			if category_name.lower() == 'exit':
				print("Back to the Menu...")
				break

			if not category_name:
				print("Invalid input Please Enter the Category Name")
				continue

			try:
				query = "INSERT INTO category(name) VALUES (%s)"
				self.db_connection.cur.execute(query,(category_name,))
				self.db_connection.conn.commit()
				print(f"Category '{category_name}' added Sucessfully.")
			except Exception as error:
				print(f"Error adding category : {error}")


	def list_category(self,show=True):
		try:
			self.db_connection.cur.execute("SELECT category_id, name FROM category ORDER BY category_id ASC")
			categories = self.db_connection.cur.fetchall()
			
			if categories:
				print("\n----------------------")
				print("Avilable Categories..")
				print("----------------------")

				if show:
					for category in categories:
						category_id, name = category
						print(f"Category ID : {category_id},  Name : {name}")
				return categories
			else:
				print("No category Found.")

		except Exception as error:
			print("Error Listing Category",error)


	def update_category(self):
		while True:
			categories = self.list_category()

			if not categories:
				print("No Category Found. Please add categories first.")
				break

			category_id = input("Enter the Category ID to update ('exit' for Category Menu ) : ")
			if category_id.lower() == 'exit':
				print("Back to manin menu...")
				break

			if category_id.isdigit():
				category_id = int(category_id)

				if any(category_id == category[0] for category in categories):
					
					new_category_name = input("Enter the New Category Name : ").strip()
					if not new_category_name:
						print("\nInvalid input, Please Enter the Category Name")
						continue

					try:
						query = "UPDATE category SET name = %s WHERE category_id = %s"
						self.db_connection.cur.execute(query,(new_category_name,category_id))
						self.db_connection.conn.commit()
						print(f"Category with ID {category_id} updatede Sucessfully.")
					except Exception as error:
						print("Error Updating Category",error)

				else:
					print("Invalid Input, Please Enter Valid Category Id from List.")

			else:
				print("Invalid input, please enter valid category id from List.")


	def delete_category(self):
		self.list_category()
		
		while True:
			category_id = input("Enter the Category ID to delete ('exit' for Category Menu) : ")
			if category_id.lower() == 'exit':
				print("Back to Admin menu...")
				break

			if category_id.isdigit():
				category_id = int(category_id)
				delete_category_obj = Utils(self.db_connection)
				delete_category_obj.delete_item("category", ['category_id', category_id ])
			else:
				print("Please Enter Valid input..")	

