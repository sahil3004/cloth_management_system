from database import DatabaseConnection
import psycopg2

class OrderManage:

	def __init__(self,db_connection,user_id=None):
		self.db_connection = db_connection
		self.user_id = user_id


	def order_management(self):
		while True:
			print("\n\n-------------------------")
			print("🛒 Order Management:")
			print("-------------------------")
			print("\n1. 📜 Display Order History")
			print("2. 🔄 Change Order Status")
			print("3. 🚪 Exit")


			choice = input("\nPlease Select an Options : ")

			if choice not in ['1','2','3']:
				print("\nInvalid Input, Please choose right choice.")
				continue

			if choice == '1':
				self.display_order_history()

			elif choice == '2':
				while True:
					orders = self.display_order_history()
					order_id = input("\nEnter the Order ID ('exit' for Back to menu): ")
					if order_id.lower() == 'exit':
						print("Returning To menu...")
						break

					if order_id.isdigit():
						order_id = int(order_id)

						selected_order = next((order for order in orders if order_id == order[0]),None)

						if selected_order:
							print(f"\nCurrent Status : {selected_order[6]}")
							print("----------------------------------------")


							if selected_order[6] == 'Pending':
								print("\nSelect The Status..\n1. ✅ Confirmed\n2. 🚚 Shipped\n3. 📦 Delivered\n4. 🚫 Canceled\n5. 🚪 Exit.")
								choice = input("\nChoose The Above Option (1-5) : ")
								if choice not in ['1','2','3','4','5']:
									print("\nInvalid Input, Please choose right choice..")
									continue

								if choice == '1':
									new_status = 'Confirmed'
									self.change_order_status(order_id,new_status)

								elif choice == '2':
									print("Cannot Shipped an order that is not Confirmed")

								elif choice == '3':
									print("Cannot deliver an order that is not shipped.")

								elif choice == '4':
									new_status = 'Canceled'
									self.change_order_status(order_id,new_status)

								elif choice == '5':
									print("\nReturning To menu...")
									break

							elif selected_order[6] == 'Confirmed':
								print("\nSelect The Status..\n1. ✅ Confirmed\n2. 🚚 Shipped\n3. 📦 Delivered\n4. 🚫 Canceled\n5. 🚪 Exit.")
								choice = input("\nChoose The Above Option (1-5) : ")
								if choice not in ['1','2','3','4','5']:
									print("\nInvalid Input, Please choose right choice..")
									continue

								if choice == '1':
									print("Order is Already Confirmed..")

								elif choice == '2':
									new_status = 'Shipped'
									self.change_order_status(order_id,new_status)

								elif choice == '3':
									print("Cannot deliver an order that is not shipped.")

								elif choice == '4':
									new_status = 'Canceled'
									self.change_order_status(order_id,new_status)

								elif choice == '5':
									print("\nReturning To menu...")
									break

							elif selected_order[6] == 'Shipped':
								print("\nSelect The Status..\n1. ✅ Confirmed\n2. 🚚 Shipped\n3. 📦 Delivered\n4. 🚫 Canceled\n5. 🚪 Exit.")
								choice = input("\nChoose The Above Option (1-5) : ")
								if choice not in ['1','2','3','4','5']:
									print("\nInvalid Input, Please choose right choice..")
									continue

								if choice == '1':
									print("Order is already Confirmed.")
								elif choice == '2':
									print("Order is already Shipped.")
								elif choice == '3':
									new_status = 'Delivered'
									self.change_order_status(order_id,new_status)
								elif choice == '4':
									new_status = "Canceled"
									self.change_order_status(order_id,new_status)
								elif choice == '5':
									print("\nReturning To Menu...")
									break


							elif selected_order[6] == 'Delivered':
								print("Order is already Delivered.")
							elif selected_order[6] == 'Canceled':
								print("Order is already Canceled.")
							else:
								print("Invalid Order Status.")
						else:
							print("Invalid Order ID, Please choose right Option form the List.")
					else:
						print("Invalid Input, Please Choose right option..")
			elif choice == '3':
				print("Back to Admin menu.")
				break


	def change_order_status(self,order_id,new_status):
		try:
			update_query = """
							UPDATE orders 
							SET status = %s
							WHERE order_id = %s
							"""

			self.db_connection.cur.execute(update_query,(new_status,order_id))
			self.db_connection.conn.commit()
			print(f"Order {order_id} status changed to {new_status}.")
		except Exception as error:
			print(f"Error changing order status: {error}")



	def display_order_history(self,user_id=None):
		try:
			query = """
					SELECT orders.order_id, users.name AS user_name, orders.date_time,products.name As product_name,products.description, products.price, orders.status, shipping_address.city, shipping_address.state
					FROM orders
					JOIN users ON orders.user_id = users.user_id	
					JOIN shipping_address ON orders.shipping_address_id = shipping_address.ship_id
					JOIN orders_details ON orders.order_id = orders_details.order_id
					JOIN products ON orders_details.product_id = products.product_id
					ORDER BY orders.date_time DESC 
					"""
			self.db_connection.cur.execute(query)
			orders = self.db_connection.cur.fetchall()

			if orders:
				print("______________________\nOrder History..📜🕰️\n______________________")
				for order in orders:
					order_id, user_name, date_time, product_name, description, price, status, city, state = order
					print("-----------------------------------------------------------------------------------------------------------------------")
					print(f"\n\nUser : {user_name}\nDate/Time : {date_time}\n Order ID : {order_id}\n\nProduct Name : {product_name}\nDescription : {description}\nPrice : {price}\n\nStatus : {status}\nShipping Address : {city}, {state}")
					print("-----------------------------------------------------------------------------------------------------------------------")

			else:
				print("No History Found.")
			return orders

		except Exception as error:
			print("Error Displaying Order History : ",error)

