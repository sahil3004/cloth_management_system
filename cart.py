from database import DatabaseConnection
from product import ProductManagement


class ViewCart:

	def __init__(self,db_connection,user_id):
		self.db_connection = db_connection
		self.user_id = user_id


	def add_cart(self,product_id,user_id, quntity=1):
		try:
			stock_query = "SELECT stock FROM products WHERE product_id = %s"
			self.db_connection.cur.execute(stock_query,(product_id,))
			stock = self.db_connection.cur.fetchone()[0]

			if stock >= quntity:
				new_stock = stock - quntity
				update_stock_query = "UPDATE products SET stock = %s WHERE product_id = %s"
				self.db_connection.cur.execute(update_stock_query, (new_stock, product_id))
				self.db_connection.conn.commit()


				query = """SELECT c.cart_id, c.product_id, c.user_id, c.quntity
							FROM cart c 
							WHERE c.product_id = %s AND c.user_id = %s"""

				self.db_connection.cur.execute(query,(product_id, self.user_id))
				cart_items = self.db_connection.cur.fetchone()

				if cart_items:
					cart_id, _, _, quntity = cart_items

					total_quantity = quntity + quntity
					update_query = "UPDATE cart SET quntity = %s WHERE product_id = %s AND user_id = %s"
					self.db_connection.cur.execute(update_query,(total_quantity,product_id,user_id))
					self.db_connection.conn.commit()
				else:
					insert_query = "INSERT INTO cart (product_id,user_id,quntity) VALUES(%s,%s,%s) "
					self.db_connection.cur.execute(insert_query,(product_id,self.user_id,quntity))
					self.db_connection.conn.commit()
				return True
			else:
				print("Sorry, the product is out of stock.")
				return False

		except Exception as error:
			print("ERROR on Adding Cart",error)
			return False


	def view_cart(self,user_id):
		try:
			query = """SELECT c.cart_id, p.product_id, p.name, c.quntity, p.price
						FROM cart c 
						JOIN products p ON c.product_id = p.product_id
						WHERE c.user_id = %s"""

			self.db_connection.cur.execute(query,(self.user_id,))
			cart_items = self.db_connection.cur.fetchall()

			if cart_items:
				total_price = 0
				print("Your Cart\n")
				for index,product in enumerate(cart_items, start = 1):
					cart_id,product_id,name,quntity,price = product
					total_price += price * quntity
					print(f"\n{index}\nName : {name}\nQuntity : {quntity}\nPrice : {price}\n")
				print(f"Total Price : {total_price}\n")

				
				while True:
					print("Select an Option....\n1. Delete Product\n2. Place Order.\n3. Exit....\n")
					choice = input("Please Select the Option (1-3) : ")
					if choice not in ['1','2','3']:
						print("\nInvalid input, please select right options...")
						continue

					if choice == '1':

						remove_option = input(f"Select any product if you want to delete (or 'exit' to go back):")
						if remove_option.lower() == 'exit':
							print("Back to main menu...")
							return

						if remove_option.isdigit():
							remove_option = int(remove_option)

							if 1 <= remove_option <= len(cart_items):
								selected_remove_product = cart_items[remove_option - 1]
								remove_product = self.remove_from_cart(selected_remove_product[0])

					if choice == '2':
						self.order_palce(user_id)

					if choice == '3':
						print("Returning to Menu...")
						break

			else:
				#print("\n-----------------------------------------------------\n               Hey, it feels so light! 🛍️\nThere is nothing in your bag. Let's add some items.\n-----------------------------------------------------")
				print()
				print("\n🌟🛍️✨  Hey, it feels so light! Let's add some items to your bag.  🌟🛍️✨")
				print("----------------------------------------------------------------------------")
				print("          🎉 There is nothing in your bag. Time to shop and save! 🎉")
				print("	-------------------------------------------------------------")
				print()		
				while True:
					print("\n___________________________\n\n1. ADD ITEMS FROM WISHLIST\n2. Exit\n___________________________")
					choice = input("\nPlease Select Above Option : ")
					if choice not in ['1','2']:
						print("Invalid Option Please Choose right option..")
						continue

					if choice == '1':
						query = "SELECT p.product_id, p.name, p.description, p.price FROM products p JOIN wishlist w ON p.product_id = w.product_id WHERE user_id =%s"
						self.db_connection.cur.execute(query,(self.user_id,))
						wishlist = self.db_connection.cur.fetchall()
						if wishlist:
							print("\nYour Wishlist...\n")
							for index, product in enumerate(wishlist, start=1):
								print(f"{index}")
								print(f"Name: {product[1]}")
								print(f"Description: {product[2]}")
								print(f"Price: INR {product[3]:.2f}\n")
								print()

							cart_product = input("Enter the index of the product to add to Cart: ")
							if cart_product.isdigit():
								cart_product = int(cart_product)

								if 1 <= cart_product <= len(wishlist):
									selected_product = wishlist[cart_product - 1]
									product_id_cart = selected_product[0]

									quantity = 1

									cart_obj = ViewCart(self.db_connection, user_id)
									cart_obj.add_cart(product_id_cart, user_id, quantity)
									print(f"\nProduct added to your Cart.")
								else:
									print("Invalid product index.")

							else:
								print("Invalid input, please enter valid integer..")
						else:
							print("\nNo Product in Wishlist...")
							break

					elif choice == '2':
						print("Back to menu..")
						break

		except Exception as error:
			print("Error on Veiw Cart",error)


	def remove_from_cart(self,cart_id):
		try:
			query = "DELETE FROM cart WHERE cart_id = %s AND user_id = %s"
			self.db_connection.cur.execute(query,(cart_id,self.user_id))
			self.db_connection.conn.commit()
			print("\nProduct Remove from Cart Sucessfully...")
		except Exception as error:
			print("Error removing product from cart:", error)

	def empty_cart(self):
		try:
			delete_query = "DELETE FROM cart WHERE user_id = %s"
			self.db_connection.cur.execute(delete_query, (self.user_id,))
			self.db_connection.conn.commit()
		except Exception as error:
			print("Error emptying cart:", error)


	def order_palce(self,user_id):
		try:

			query = """SELECT c.cart_id, p.product_id, p.name, c.quntity, p.price, p.stock
					FROM cart c 
					JOIN products p ON c.product_id = p.product_id
					WHERE c.user_id = %s"""

			self.db_connection.cur.execute(query, (user_id,))
			cart_items = self.db_connection.cur.fetchall()

			if not cart_items:
				print("Your Cart is empty. Cannot place an order.")
				return

			for _, product_id, _, quantity, price, stock in cart_items:
				
				if quantity > stock:
					print(f"Sorry, the product with ID {product_id} is out of stock. Please adjust the quantity in your cart.")
					return

			check_address_query = "SELECT ship_id,city,state,postal_code FROM shipping_address WHERE user_id = %s"
			self.db_connection.cur.execute(check_address_query,(user_id,))
			exiting_address = self.db_connection.cur.fetchone()

			if exiting_address:
				ship_id, city, state, postal_code = exiting_address
				print("\nYour Existing Shipping Address:")
				print(f"\nShip ID: {ship_id}")
				print(f"City: {city}")
				print(f"State: {state}")
				print(f"Postal Code: {postal_code}")

				update_address = input("\nDo you want to update your shipping address? (yes/no): ")
				if update_address.lower() == 'yes':

					print("\nEnter Updated Shipping Address:")
					city = input("City: ")
					state = input("State: ")
					postal_code = input("Postal Code: ")

					update_address_query = "UPDATE shipping_address SET city = %s,  state = %s, postal_code = %s WHERE ship_id = %s"
					self.db_connection.cur.execute(update_address_query,(city,state,postal_code,ship_id))
					self.db_connection.conn.commit()

			else:
				print("\nEnter Shipping Address:")
				city = input("City: ")
				state = input("State: ")
				postal_code = input("Postal Code: ")

				insert_address_query = "INSERT INTO shipping_address (user_id, city, state, postal_code) VALUES (%s, %s, %s, %s) RETURNING ship_id"
				self.db_connection.cur.execute(insert_address_query, (user_id, city, state, postal_code))
				ship_id = self.db_connection.cur.fetchone()[0]
				self.db_connection.conn.commit()

			print("\nOrder Summary: 👕")

			total_price = 0
			for index, product in enumerate(cart_items, start=1):
				_, _, name, quantity, price,stock = product
				total_price += price * quantity
				print(f"\n{index}\nName : {name}\nQuantity : {quantity}\nPrice : {price}\n")
			print(f"Total Price : {total_price}")

			for _, product_id, _, quantity, _, stock in cart_items:
				if quantity > stock:
					print(f"Sorry, the product with ID {product_id} is out of stock. Please adjust the quantity in your cart.")
					return

			# Get the user's discounts
			user_discounts_query = "SELECT d.discount_id, d.coupon_code, d.discount_amount FROM discounts d WHERE d.product_id IN (SELECT c.product_id FROM cart c WHERE c.user_id = %s)"
			self.db_connection.cur.execute(user_discounts_query, (user_id,))
			user_discounts = self.db_connection.cur.fetchall()

			# Check if the user has any applicable discounts
			if user_discounts:
				print("\nYou have the following discounts:")
				for discount in user_discounts:
					discount_id, coupon_code, discount_amount = discount
					print(f"{coupon_code}: INR {discount_amount:.2f}")

				apply_discount = input("\nDo you want to apply a discount? (yes/no): ")
				if apply_discount.lower() == 'yes':
					coupon_code_input = input("Enter the coupon code: ")

					# Find the discount with the entered coupon code
					selected_discount = None
					for discount in user_discounts:
						_, coupon_code, discount_amount = discount
						if coupon_code == coupon_code_input:
							selected_discount = discount
							break

					if selected_discount:
						discount_id, _, discount_amount = selected_discount

						# Apply the discount to the total price
						total_price -= discount_amount

						# Mark the coupon as used
						update_discount_query = "UPDATE discounts SET is_used = TRUE WHERE discount_id = %s"
						self.db_connection.cur.execute(update_discount_query, (discount_id,))
						self.db_connection.conn.commit()

						print()
						print("********************************************************************************")
						print(f"\nDiscount of INR {discount_amount:.2f} applied successfully!")
						print(f"🎉✨🛍️ Discount Applied Successfully! Enjoy Savings of INR {discount_amount:.2f}! 🛍️✨🎉")
						print("********************************************************************************")
					else:
						print("Invalid coupon code. Discount not applied.")

			# Confirm order
			confirm_order = input("\nDo you want to confirm the order? (yes/no): ")
			if confirm_order.lower() != 'yes':
				print("Order cancelled. ❌")
				return

			# Insert order details into the database
			insert_order_query = "INSERT INTO orders (user_id, date_time, amount, status, shipping_address_id,coupon_code) VALUES (%s, NOW(), %s, 'Pending', %s ,%s) RETURNING order_id"
			self.db_connection.cur.execute(insert_order_query, (user_id, total_price, ship_id, None,))
			order_id = self.db_connection.cur.fetchone()[0]
			self.db_connection.conn.commit()

			for product in cart_items:
				_, product_id, _, quantity, price, stock = product

				update_stock_query = "UPDATE products SET stock = stock - %s WHERE product_id = %s"
				self.db_connection.cur.execute(update_stock_query, (quantity, product_id))
				self.db_connection.conn.commit()

				
				insert_order_detail_query = "INSERT INTO orders_details (order_id, product_id, quntity, amount,discount_amount) VALUES (%s, %s,%s, %s, %s)"
				self.db_connection.cur.execute(insert_order_detail_query, (order_id, product_id, quantity, price * quantity,None))
				self.db_connection.conn.commit()

			# Empty the cart after placing the order
			self.empty_cart()

			print()
			print()
			print("🌟✨🛍️  Order Placed Successfully! Thank you for choosing us!  🛍️✨🌟")			 
			print("************************************************************************")
			print("             ********  Enjoy Your Shopping!  ********")
			print()
			print()
		except Exception as error:
			print("Error placing order:", error)



