from database import DatabaseConnection
from user_ragister import UserRegister
from user_login import Userlogin
from admin import AdminDashboard
from client import ClientMenu
from util import Utils
import getpass


def main():
	hostname = 'localhost'
	database = 'shopping'
	username = 'postgres'
	pwd = 'root'
	port_id = 5432

	db = DatabaseConnection(hostname, database, username, pwd, port_id)
	db.connect()

	while True:
		print("-------------------------------------------------")
		print("🌟 Welcome to LUXIRIS Clothing Store! 🛍️👗👔✨")
		print("-------------------------------------------------")
		print("\n1. Admin")
		print("\n2. Client")
		print("\n3. EXIT🚪")
		print()
		choice = input("Please Select an option (1-3): ")

		if choice not in ['1','2','3']:
			print("Invalid option Please try again.")
			continue

		if choice == '1':
			while True:
				print("\n\n----------------")
				print("Admin Menu  🛠️:")
				print("----------------")
				print("\n1. Login ")
				print("\n2. New Admin Register")
				print("\n3. Back to Main Menu")

				admin_choice = input("\nPlease Select an option (1-3): ")
				if admin_choice not in ['1','2','3']:
					print("Invalid input Please try again.")
					continue

				if admin_choice == '1':
					while True:
						username = input("\nEnter username: ")
						if len(username) <= 8 and username.isalpha():
							password = getpass.getpass("Enter Password: ")
							user_login = Userlogin(db)
							user_data = user_login.admin_login(username,password)
							if user_data:
								print("Logging in... 🔐🔄")
								admin_dashboard = AdminDashboard(db)
								admin_dashboard.admin_menu(user_data[0])
								break
							else:
								print("Admin login ❌ Failed.")
								break
						else:
							print("\nUser name must be alphabet characters and Length is less then 8...")

				elif admin_choice == '2':
					print("✨ Create New Account ✨")
					while True:
						username = input("\nEnter Username: ")
						if len(username) <= 8:
							password = input("Enter Password: ")
							email = input("Enter Email: ")
							valid_email = Utils(db)
							if valid_email.is_valid_email(email):
								user_ragister = UserRegister(db)
								if user_ragister.admin_register(username,password,email):
									print("Admin registration ✅ successful.")
									break
								else:
									print("Admin registration ❌ failed.")
									break
							else:
								print("\n Invalid Email, please enter the right email.")
						else:
							print("\nUser name Length must be less then 8...")

				elif admin_choice == '3':
					break

				else:
					print("Invalid Choice, Please select a valid option. :(")


		elif choice == '2':
			while True:
				print("\nClient Menu:")
				print("\n1. Login")
				print("2. Create New Account")
				print("3. Back to Main Menu")

				client_choice = input("\nPlease Select an Option (1-3) : ")
				if client_choice not in ['1','2','3']:
					print("Invalid input Please try again.")
					continue

				if client_choice == '1':
					while True:	
						username = input("\nEnter username: ")
						if username.isalpha() and len(username) <= 8:
							password = getpass.getpass("Enter password: ")
							user_login = Userlogin(db)
							user_data = user_login.client_login(username,password)
							if user_data:
								print("Logging in... 🔐🔄")
								client_data = ClientMenu(db)
								client_data.client_dashboard(user_data[0])
								break
							else:
								print("Client login ❌ Failed.")
								break
						else:
							print("\nUser name must be alphabet characters and Length is less then 8...")


				elif client_choice == '2':
					while True:

						username = input("\nEnter Username: ")
						password = input("Enter Password: ")
						email = input("Enter Email: ")

						valid_email = Utils(db)
						if valid_email.is_valid_email(email):
							user_ragister = UserRegister(db)
							
							if user_ragister.client_register(username,password,email):
								print("Client registration ✅ successful.")
								break
							else:
								print("Client registration ❌ failed.")

						else:
							print("Invalid Email, Please enter the right email.")

				elif client_choice == "3":
					print("Back to Main Menu...")
					break

				else:
					print("Invalid Choice, Please select a valid option. :(")


		elif choice == "3":
			print("\nExiting......")
			break

		else:
			print("Invalid Choice, Please select a valid option. :(")

	db.close_connection()

if __name__ == "__main__":
	main()


	


