import psycopg2
from database import DatabaseConnection

class Userlogin:

	def __init__(self,db_connection):
		self.db_connection = db_connection


	def admin_login(self, name, password):
		try:
			query = "SELECT * FROM users WHERE name = %s AND password = %s AND user_type = %s"
			self.db_connection.cur.execute(query,(name, password, True))
			result = self.db_connection.cur.fetchone()
			if result:
				return result
			else:
				return False
		except Exception as error:
			print("Error during admin login",error)
			return False


	def client_login(self,name,password):
		try:
			query = "SELECT * FROM users WHERE name = %s AND password = %s AND user_type = %s"
			self.db_connection.cur.execute(query,(name, password, False))
			result = self.db_connection.cur.fetchone()
			if result:
				return result
			else:
				return False
		except Exception as error:
			print("Error during Client Login",error)
			return False