from database import DatabaseConnection
import psycopg2
from psycopg2 import sql

class SalesReport:

	def __init__(self,db_connection):
		self.db_connection = db_connection


	def report_display(self):
		count = self.count_order_by_coustmer()
		total_sales = self.calculate_total_sales()
		
		print("\n\n-----------------------------------------------")
		print(f"Number of orders placed by customers: {count}")
		if total_sales is not None:
			print(f"Total sales: INR {total_sales:.2f}")
			print("-----------------------------------------------")
		else:
			print("Total sales not avilable.")


	def calculate_total_sales(self):
		try:
			query = """SELECT SUM(orders.amount)
						FROM orders
						WHERE orders.status = 'Delivered' 
					"""
			 
			self.db_connection.cur.execute(query)
			result = self.db_connection.cur.fetchone()

			if result:
				return result[0]
			else:
				return "No order Found"
		except Exception as error:
			return "Error Calculating Total Sales: "+ str(error)


	def count_order_by_coustmer(self):
		try:
			query = sql.SQL(""" SELECT COUNT(orders.order_id)
								FROM orders
								WHERE orders.status IN ({},{},{})""").format(sql.Literal("Confirmed"),sql.Literal("Delivered"),sql.Literal("Shipped"))

			self.db_connection.cur.execute(query)
			result = self.db_connection.cur.fetchone()

			if result:
				return result[0]
			else:
				return 0

		except Exception as error:
			print("Error Counting Order By Coustmer:",error)
			return 0


	