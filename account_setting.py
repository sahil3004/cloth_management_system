from database import DatabaseConnection
import getpass
from util import Utils

class AccountSettings:

	def __init__(self,db_connection, user_id):
		self.db_connection = db_connection
		self.user_id = user_id


	def account_setting_menu(self):
		while True:
			print("\n⚙️Account Settings Menu:")
			print("---------------------------")
			print("\n1. Update Username")
			print("2. Update Password")
			print("3. Update Email")
			print("4. Account Details.")
			print("5. Back to Previous Menu")

			choice = input("\nPlease select an Option : ")

			if choice not in ['1','2','3','4','5']:
				print("Invalid input, please choose right option.")
				continue

			if choice == '1':
				self.change_username()

			elif choice == '2':
				self.change_password()

			elif choice == '3':
				self.change_email()

			elif choice == '4':
				self.list_account()

			elif choice == '5':
				print("\nReturning To Admin menu..")
				break


	def change_username(self):
		self.list_account()
		while True:
			new_username = input("\nEnter the New User Name : ").strip()
			if not new_username.isalpha() and len(new_username) <= 8:
				print("\n⚠️User name Must be Alphabetical and lenght of user name is less than equal to 8.")
				continue
			break

		try:
			update_query = f"UPDATE users SET name = '{new_username}' WHERE user_id = '{self.user_id}'"
			self.db_connection.cur.execute(update_query)
			self.db_connection.conn.commit()
			print(f"\n✅ Username changed successfully.\n Updated user name is : {new_username}.")
			return True
		except Exception as error:
			print("Error Updating Username",error)
			return False


	def change_password(self):
		while True:
			new_password = input("\nEnter the new Password : ").strip()
			if not len(new_password) <= 8:
				print("Password Must be Less then 8 letter..")
				continue

			confirm_pass = input("\nConfirm Password : ").strip()
			if not new_password == confirm_pass:
				print("\n⚠️ Password change unsuccessful. Please try again.")
				continue

			try:
				update_query = f"UPDATE users SET password = '{new_password}' WHERE user_id = '{self.user_id}'"
				self.db_connection.cur.execute(update_query)
				self.db_connection.conn.commit()
				print("\n✅ Password changed successfully.")
				return True
			except Exception as error:
				print("Error updating password:", error)
				return False
			break



	def change_email(self):
		self.list_account()
		new_email = input("\nEnter the new Email : ")
		util_obj = Utils(self.db_connection)
		util_obj.is_valid_email(new_email)
		try:
			update_query = f"UPDATE users SET email = '{new_email}' WHERE user_id = '{self.user_id}'"
			self.db_connection.cur.execute(update_query)
			self.db_connection.conn.commit()
			self.list_account()
			return True
		except Exception as error:
			print("Error updating Email:", error)
			return False


	def list_account(self):
		try:
			query = f"SELECT name,password,email FROM users WHERE user_id = {self.user_id}"
			self.db_connection.cur.execute(query)
			account_details = self.db_connection.cur.fetchall()

			for info in account_details:
				print(f"\nUser Name : {info[0]}")
				print(f"Email : {info[2]}")

		except Exception as error:
			print("Error on listing Account Details",error)


