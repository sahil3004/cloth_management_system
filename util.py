import psycopg2
from database import DatabaseConnection
from datetime import datetime
import re

class Utils:

	def __init__(self,db_connection):
		self.db_connection = db_connection

	def delete_item(self,table_name,field_data):
		try:
			query = f"DELETE FROM {table_name} WHERE {field_data[0]} = {field_data[1]}"
			self.db_connection.cur.execute(query)
			self.db_connection.conn.commit()
			print(f"{table_name.capitalize()} with ID {field_data[1]} deleted Sucessfully!")
		except Exception as error:
			print(f"Error Deleting {table_name}: {error}")


	def drop_column(self,table_name,table_coloumn):
		try:
			query = f"ALTER TABLE {table_name} DROP COLUMN {table_coloumn}"
			self.db_connection.cur.execute(query)
			self.db_connection.conn.commit()
			print(f"{table_coloumn} Drop Sucessfully. ")
		except Exception as error:
			print("ERROR DROPPING COLOUMN...")


	def is_valid_date(self,date_str):
		try:
			datetime.strptime(date_str,"%Y-%m-%d")
			return True
		except ValueError:
			return False
			

	def is_valid_start_end_date(self,start_date,end_date):
		return start_date <= end_date


	def is_valid_email(self,email):
		regex = re.compile(r'([A-Za-z0-9]+[.-_])*[A-Za-z0-9]+@[A-Za-z0-9-]+(\.[A-Z|a-z]{2,})+')  

		if re.fullmatch(regex,email):
			print("Given Email is Valid..")
		else:
			print("Given Email is Invalid..")



