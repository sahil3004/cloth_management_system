from database import DatabaseConnection
from product import ProductManagement
from cart import ViewCart
import psycopg2

class Wishlist:

    def __init__(self,db_connection,user_id):
        self.db_connection = db_connection
        self.user_id = user_id


    def add_wishlist(self,product_id,user_id):
        try:
            query = """SELECT w.wishlist_id, w.product_id, w.user_id
                        FROM wishlist w
                        WHERE w.product_id = %s AND w.user_id = %s"""

            self.db_connection.cur.execute(query,(product_id, self.user_id))
            wishlist_items = self.db_connection.cur.fetchone()

            if wishlist_items:
                print("Product already in Wishlist")
                return False
            else:
                query = "INSERT INTO wishlist (user_id,product_id) VALUES (%s,%s)"
                self.db_connection.cur.execute(query,(self.user_id,product_id,))
                self.db_connection.conn.commit()
                print(f"\nProduct added into your wishlist")
            return True
        except Exception as error:
            print("Error Adding Wishlist",error)
            return False


    def list_wishlist(self,user_id):
        try:
            query = "SELECT p.product_id, p.name, p.description, p.price FROM products p JOIN wishlist w ON p.product_id = w.product_id WHERE user_id =%s"
            self.db_connection.cur.execute(query,(self.user_id,))
            wishlist = self.db_connection.cur.fetchall()
            if wishlist:
                print("\nYour Wishlist...\n")
                for index, product in enumerate(wishlist, start=1):
                    print(f"{index}")
                    print(f"Name: {product[1]}")
                    print(f"Description: {product[2]}")
                    print(f"Price: INR {product[3]:.2f}\n")
                    print()

                while True:
                    action = input("Select an action:\n1. Delete from Wishlist\n2. Add to Cart\n3. Back to menu\n\nSelect the above option : ")

                    if action not in ['1','2','3']:
                        print("Invalid Option Please enter the valid input..")
                        continue

                    if action == '1':
                        product_index = input("Enter the index of the product to delete: ")
                        if product_index.isdigit():
                            product_index = int(product_index)

                            if 1 <= product_index <= len(wishlist):
                                selected_product = wishlist[product_index - 1]
                                product_id_delete = selected_product[0]
                                self.remove_wishlist(product_id_delete)
                                print("Product removed from Wishlist.")
                            else:
                                print("Invalid product index.")
                        else:
                            print("\n Invalid input, Enter valid integer.")


                    elif action == '2':
                        cart_product = input("Enter the index of the product to add to Cart: ")
                        if cart_product.isdigit():
                            cart_product = int(cart_product)

                            if 1 <= cart_product <= len(wishlist):
                                selected_product = wishlist[cart_product - 1]
                                product_id_cart = selected_product[0]

                                quantity = 1

                                cart_obj = ViewCart(self.db_connection, user_id)
                                cart_obj.add_cart(product_id_cart, user_id, quantity)
                                print(f"\nProduct added to your Cart.")
                                self.remove_wishlist(product_id_cart)
                            else:
                                print("Invalid product index.")

                        else:
                            print("Invalid input, please enter valid integer..")


                    elif action == '3':
                        print("Back to main menu...")
                        break
            else:
                print("\nYour Wishlist is empty... ❌")
        except Exception as error:
            print("ERROR Listing Wishlist",error)


    def remove_wishlist(self,product_id):

        query = "DELETE FROM wishlist WHERE user_id = %s AND product_id =%s"
        try:    
            self.db_connection.cur.execute(query,(self.user_id,product_id,))
            self.db_connection.conn.commit()
            return True
        except Exception as e:
            print("ERROR Remove Wishlist.",e)
