import psycopg2
from psycopg2 import sql
from database import DatabaseConnection
from category_manage import CategoryManagement
from sub_category import SubCategory
from util import Utils

class ProductManagement:

	def __init__(self,db_connection):
		self.db_connection = db_connection

	def product_management(self):
		while True:
			print("\n 📦Product Management:")
			print("\n1. Add Product")
			print("2. Update Product")
			print("3. Delete Product")
			print("4. List Product")
			print("5. Back To Admin Menu")

			choice = input("Please Select an option(1-5) : ")
			if choice not in ['1','2','3','4','5']:
				print("Invalid option please Try again..")
				continue

			if choice == '1':
				self.add_product()
			elif choice == '2':
				self.update_product()
			elif choice == '3':
				self.delete_product()
			elif choice == '4':
				self.list_product()
			elif choice == '5':
				print("Returning to Admin menu..")
				break


	def list_product(self):
		try:
			query = """ SELECT products.product_id, products.name, products.description, category.name AS category_name, products.price,size.name AS size_name, products.stock 
						FROM products LEFT JOIN category ON products.category_id = category.category_id LEFT JOIN size ON products.size_id = size.size_id
					"""
			self.db_connection.cur.execute(query)
			products = self.db_connection.cur.fetchall()
			if products:
				print("\nAvilable Products..")
				print()
				for index, product in enumerate(products, start=1):
					product_id, name, description, category_name,size_name, price, stock = product
					print(f"{index}\nProduct ID : {product_id}\nName : {name}\nDescription : {description}\nCategory : {category_name}\nSize : {size_name}\nPrice : {price}\nStock : {stock}")
					print()				
				return products
			else:
				print("No Products Found...")
		except Exception as error:
			print("Error listing products: ",error)
			return []



	def delete_product(self):
		while True:
			self.list_product()

			product_id = input("Enter the Product id to Delete ('exit' for Back to Admin Menu) : ")
			if product_id.lower() == 'exit':
				print("Back TO Admin Menu...")
				break

			if product_id.isdigit():
				product_id = int(product_id)

				delete_product = Utils(self.db_connection)
				delete_product.delete_item('wishlist',['product_id', product_id])
				delete_product.delete_item('cart',['product_id',product_id])
				delete_product.delete_item('products',['product_id',product_id])
			else:
				print("Please Enter Valid Input.")


	def update_product_field(self,product_id,field,new_value):
		try:
			update_query = f"UPDATE products SET {field} = %s WHERE product_id = %s"
			self.db_connection.cur.execute(update_query,(new_value,product_id))
			self.db_connection.conn.commit()
			print(f"Product with ID {product_id} updated successfully!")
		except Exception as error:
			print(f"Error updating product: {error}")

		
	def update_product(self):
		products = self.list_product()

		if not products:
			print("NO Products Available...")
			return

		while True:
			try:
				selected_product_index = int(input("\nSelect the product to update (enter the index): "))
				if 1 <= selected_product_index <= len(products):
					selected_product_id = products[selected_product_index - 1][0]
					break
				else:
					print("Invalid product index. Please choose a valid index.")
			except ValueError:
				print("Invalid input. Please enter a valid product index.")

		while True:
			print("\n1. Change Product Name.")
			print("2. Change Product Description.")
			print("3. Change Product Size.")
			print("4. Change Product Price.")
			print("5. Change Product Stock.")
			print("6. Exit.")

			choice = input("Please select an option (1-8): ")

			if choice == '1':
				while True:
					new_product_name = input("Enter the new product name: ").strip()
					if not new_product_name:
						print("Invalid Product Name. Please enter a non-empty name.")
						continue
					self.update_product_field(selected_product_id, 'name', new_product_name)
					break
			elif choice == '2':
				while True:
					new_description = input("Enter the new product description: ").strip()
					if not new_description:
						print("Invalid Product Name. Please enter a non-empty name.")
						continue
					self.update_product_field(selected_product_id, 'description', new_description)
					break
			elif choice == '3':
				while True:
					self.size_list()
	
					new_product_size = input("Enter the new product size ('exit' for back menu): ")
					if new_product_size.lower() == 'exit':
						print("Returning back to menu..")
						break
					if not new_product_size.isdigit():
						print("Please Enter Valid numeric value for size.")
						continue
					new_product_size = int(new_product_size)
					self.update_product_field(selected_product_id,'size_id',new_product_size)
					break


			elif choice == '4':
				while True:
					new_price = input("Enter the new product price : ")
					if not new_price.isdigit():
						print("Invalid Input. Please Enter valid numeric vlaue for price.")
						continue
					new_price = int(new_price)
					if new_price <= 0:
						print("Price Must be Greater then Zero.")
						continue
					self.update_product_field(selected_product_id,'price',new_price)
					break


			elif choice == '5':
				while  True:
					new_stock = input("Enter the new product Stock : ")
					if not new_stock.isdigit():
						print("Invalid Input. Please Enter valid numeric vlaue for Stock.")
						continue
					new_stock = int(new_stock)
					if new_stock <= 0:
						print("Stock Must be Greater then Zero.")
						continue
					self.update_product_field(selected_product_id,'stock',new_stock)
					break

			elif choice == '6':
				print("Exiting...")
				break
			else:
				print("Invalid choice. Please select a valid option.")


	def add_product(self):
		category_manager = CategoryManagement(self.db_connection)
		categories = category_manager.list_category(show = False)
	
		if not categories:
			print("No categories found. Please add categories first.")
			return

		while True:
			try:
				print("\nCategories:\n")
				for index, category in enumerate(categories, start=1):
					category_id, category_name = category
					print(f"{index}. {category_name}")

				selected_category_index = int(input("\nSelect a category : "))
				
				if 1 <= selected_category_index <= len(categories):
					selected_category_id = categories[selected_category_index - 1][0]
					break
				else:
					print("Invalid category selection. Please choose a valid category.")

			except ValueError:
				print("Invalid input. Please enter a valid category selection.")


		subcategories_obj = SubCategory(self.db_connection)
		subcategories = subcategories_obj.list_sub_category(category_id = selected_category_index, show = False)

		if not subcategories:
			print("No subcategories found for the selected category.")
			return

		while True:
			try:
				print("--------------")
				print("Subcategories:")
				print("--------------")
				for index, subcategory in enumerate(subcategories, start=1):
					sub_category_id, name = subcategory
					print(f"{index}. {name}")

				selected_subcategory_index = input(f"\nSelect a subcategory (1 to {len(subcategories)}) ('exit' For returning): ")
				if selected_subcategory_index.lower() == 'exit':
					print("Returning back to menu..")
					return
				if selected_subcategory_index.isdigit():
					selected_subcategory_index = int(selected_subcategory_index)


					if 1 <= selected_subcategory_index <= len(subcategories):
						selected_subcategory_id = subcategories[selected_subcategory_index - 1][0]
						break
				else:
					print("Invalid subcategory selection. Please choose a valid subcategory.")

			except ValueError:
				print("Invalid input. Please enter a valid subcategory selection.")


		while True:
			product_name = input("Enter the Product Name: ").strip()
			if not product_name:
				print("Invalid Product Name. Please enter a non-empty name.")
			else:
				break

		while True:
			description = input("Enter Product Description: ").strip()
			if not description:
				print("Invalid Description. Please enter a non-empty description.")
			else:
				break

		while True:
			try:
				price = float(input("Enter Product Price: "))
				if price <= 0:
					print("Price should be a positive number. Please try again.")
				else:
					break
			except ValueError:
				print("Invalid input. Please enter a valid numeric value for the price.")

		self.size_list()
		while True:
			size_input = input("Please Select The Size ('exit' for back menu): ")
			if size_input.lower() == 'exit':
				print("Returning back to menu..")
				break
			if not size_input.isdigit():
				print("Invalid Input. Please select the right option.")
			else:
				size_input = int(size_input)
				break

		while True:
			try:
				stock = int(input("Enter Product Stock: "))
				if stock <= 0 :
					print("Stock must be Greater then Zero.")
			except ValueError:
				print("Invalid Input. Please enter the valid numeric value for the price.")
			break					


		try:
			query = "INSERT INTO products(name, description, category_id, sub_category_id, size_id, price, stock) VALUES (%s, %s, %s, %s, %s,%s, %s)"
			self.db_connection.cur.execute(query, (product_name, description, selected_category_id, selected_subcategory_id,size_input, price, stock))
			self.db_connection.conn.commit()
			print(f"Product '{product_name}' added successfully.")
		except Exception as error:
			print(f"Error adding product: {error}")


	def size_list(self):
		try:
			query = "SELECT size_id,name FROM size "
			self.db_connection.cur.execute(query)
			sizes = self.db_connection.cur.fetchall()

			if sizes:
				print("\nSize Avilable...")
				for index,size in enumerate(sizes,start = 1):
					size_id,name = size
					print(f"{index}. {name}")
				return sizes
			else:
				print("Sizes Not Found...")
		except Exception as error:
			print("ERROR on Listing Sizes.",error)

