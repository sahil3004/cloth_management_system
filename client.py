from database import DatabaseConnection
import psycopg2
from browse_cloth import BrowseClothing
from cart import ViewCart
from wishlist import Wishlist
from order import OrderManage
from account_setting import AccountSettings


class ClientMenu:

	def __init__(self,db_connection):
		self.db_connection = db_connection


	def client_dashboard(self,user_id):
		while True:
			print("\n---------------\nClient Menu: \n---------------")
			print("\n1. 👔 Browse Men's Clothing")
			print("2. 👗 Browse Women's Clothing")
			print("3. 🛍️ SHOPPING BAG")
			print("4. 📦 Orders")
			print("5. ❤️ Wishlist")
			print("6. ⚙️ Account Settings")
			print("7. 🚪 Logout")


			choice = input("\nPlease Choose the Option : ")
			if choice not in ['1','2','3','4','5','6','7']:
				print("Invalid Choice Please Choose Right Option between 1 to 9.")
				continue

			if choice == '1':
				browse_obj = BrowseClothing(self.db_connection,user_id)
				browse_obj.browse_men_clothing(user_id)

			elif choice == '2':
				browse_woman_cloth = BrowseClothing(self.db_connection, user_id)
				browse_woman_cloth.browse_women_clothing(user_id)

			elif choice == '3':
				view_cart_obj = ViewCart(self.db_connection,user_id)
				view_cart_obj.view_cart(user_id)

			elif choice == '4':
				order_obj = OrderManage(self.db_connection, user_id)
				order_obj.display_order_history(user_id)


			elif choice == '5':
				wishlist_obj = Wishlist(self.db_connection,user_id)
				wishlist_obj.list_wishlist(user_id)


			elif choice == '6':
				account_setting_obj = AccountSettings(self.db_connection,user_id)
				account_setting_obj.account_setting_menu()


			elif choice == '7':
				print("Logging Out...")
				break
				
