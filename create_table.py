from database import DatabaseConnection
import psycopg2
from util import Utils


class CreateTable:

	def __init__(self,db_connection):
		self.db_connection = db_connection

	def create_user_table(self):
		create_script = ''' CREATE TABLE IF NOT EXISTS users(
							user_id serial PRIMARY KEY,
							name varchar(40) NOT NULL,
							password varchar(40) NOT NULL,
							email varchar(100) NOT NULL,
							user_type boolean NOT NULL)'''

		try:
			self.db_connection.cur.execute(create_script)
			self.db_connection.conn.commit()
		except Exception as error:
			print(error)


	def create_cetegory_table(self):
		create_script = ''' CREATE TABLE IF NOT EXISTS category(
							category_id serial PRIMARY KEY,
							name varchar(40) NOT NULL)'''

		try:
			self.db_connection.cur.execute(create_script)
			self.db_connection.conn.commit()
		except Exception as error:
			print(error)


	def create_product_table(self):
		create_script = ''' CREATE TABLE IF NOT EXISTS products(
							product_id serial PRIMARY KEY,
							name varchar(40) NOT NULL,
							description text,
							category_id int NOT NULL,
							price numeric(10, 2) NOT NULL,
							stock int NOT NULL,
							FOREIGN KEY (category_id) REFERENCES category(category_id))'''

		try:
			self.db_connection.cur.execute(create_script)
			self.db_connection.conn.commit()
		except Exception as error:
			print(error)


	def create_order_table(self):
		create_script = ''' CREATE TABLE IF NOT EXISTS orders(
							order_id serial PRIMARY KEY,
							user_id int NOT NULL,
							date_time timestamp NOT NULL,
							amount numeric(10,2) NOT NULL,
							status varchar(40) NOT NULL,
							shipping_address_id int NOT NULL,
							FOREIGN KEY (user_id) REFERENCES users(user_id),
							FOREIGN KEY (shipping_address_id) REFERENCES shipping_address(ship_id))'''

		try:
			self.db_connection.cur.execute(create_script)
			self.db_connection.conn.commit()
		except Exception as error:
			print(error)


	def create_shipping_address_table(self):
		create_script = ''' CREATE TABLE IF NOT EXISTS shipping_address(
							ship_id serial PRIMARY KEY,
							user_id int NOT NULL,
							city varchar(50) NOT NULL,
							state varchar(50) NOT NULL,
							postal_code varchar(50) NOT NULL,
							FOREIGN KEY (user_id) REFERENCES users(user_id))'''

		try:
			self.db_connection.cur.execute(create_script)
			self.db_connection.conn.commit()
		except Exception as error:
			print(error)


	def create_order_detail_table(self):
		create_script = ''' CREATE TABLE IF NOT EXISTS orders_details(
							order_details_id serial PRIMARY KEY,
							order_id int NOT NULL,
							product_id int NOT NULL,
							quntity int NOT NULL,
							amount numeric(10,2) NOT NULL,
							FOREIGN KEY (order_id) REFERENCES orders(order_id),
							FOREIGN KEY (product_id) REFERENCES products(product_id))'''

		try:
			self.db_connection.cur.execute(create_script)
			self.db_connection.conn.commit()
		except Exception as error:
			print(error)


	def create_wishlist_table(self):
		create_script = ''' CREATE TABLE IF NOT EXISTS wishlist(
							wishlist_id serial PRIMARY KEY,
							product_id int NOT NULL,
							user_id int NOT NULL,
							FOREIGN KEY (product_id) REFERENCES products(product_id),
							FOREIGN KEY (user_id) REFERENCES users (user_id))'''

		try:
			self.db_connection.cur.execute(create_script)
			self.db_connection.conn.commit()
		except Exception as error:
			print(error)


	def create_cart_table(self):
		create_script = ''' CREATE TABLE IF NOT EXISTS cart(
							cart_id serial PRIMARY KEY,
							product_id int NOT NULL,
							user_id int NOT NULL,
							quntity int NOT NULL,
							FOREIGN KEY (product_id) REFERENCES products(product_id),
							FOREIGN KEY (user_id) REFERENCES users(user_id))'''

		try:
			self.db_connection.cur.execute(create_script)
			self.db_connection.conn.commit()
		except Exception as error:
			print(error)


	def create_size_table(self):
		create_script = ''' CREATE TABLE IF NOT EXISTS size(
							size_id serial PRIMARY KEY,
							name varchar(30) NOT NULL)'''

		try:
			self.db_connection.cur.execute(create_script)
			self.db_connection.conn.commit()
		except Exception as error:
			print(error)


	def create_sales_report_table(self):
		create_script = ''' CREATE TABLE IF NOT EXISTS sales_report(
							report_id serial PRIMARY KEY,
							name varchar(40) NOT NULL,
							date_time timestamp NOT NULL,
							order_id int NOT NULL,
							product_id int NOT NULL,
							FOREIGN KEY (order_id) REFERENCES orders(order_id),
							FOREIGN KEY (product_id) REFERENCES products(product_id))'''

		try:
			self.db_connection.cur.execute(create_script)
			self.db_connection.conn.commit()
		except Exception as error:
			print(error)


	def create_discount_table(self):
		create_script = ''' CREATE TABLE IF NOT EXISTS discounts(
							discount_id serial PRIMARY KEY,
							product_id int NOT NULL,
							coupon_code varchar(40) NOT NULL,
							discount_amount numeric(10,2) NOT NULL,
							start_date date NOT NULL,
							end_date date NOT NULL,
							FOREIGN KEY (product_id) REFERENCES products(product_id))'''

		try:
			self.db_connection.cur.execute(create_script)
			self.db_connection.conn.commit()
		except Exception as error:
			print(error)


	def create_sub_category_table(self):
		create_script = ''' CREATE TABLE IF NOT EXISTS sub_category(
							sub_category_id serial PRIMARY KEY,
							name varchar(40) NOT NULL,
							category_id int NOT NULL,
							product_id int NOT NULL,
							FOREIGN KEY (category_id) REFERENCES category(category_id),
							FOREIGN KEY (product_id) REFERENCES products(product_id))'''

		try:
			self.db_connection.cur.execute(create_script)
			self.db_connection.conn.commit()
		except Exception as error:
			print("Error Create Sub category table",error)


	def add_size_table(self):
		add_coloumn = ''' ALTER TABLE products
							ADD COLUMN IF NOT EXISTS size_id int,
							ADD FOREIGN KEY (size_id) REFERENCES size(size_id) '''

		try:
			self.db_connection.cur.execute(add_coloumn)
			self.db_connection.conn.commit()
		except Exception as error:
			print("Error Add Size table to products",error)


	def add_product_table(self):
		add_coloumn = ''' ALTER TABLE products
							ADD COLUMN IF NOT EXISTS sub_category_id int,
							ADD COLUMN IF NOT EXISTS discount_id int,
							ADD FOREIGN KEY (sub_category_id) REFERENCES sub_category(sub_category_id)
							ADD FOREIGN KEY (discount_id) REFERENCES discounts(discount_id)'''

		try:
			self.db_connection.cur.execute(add_coloumn)
			self.db_connection.conn.commit()
		except Exception as error:
			print("Error Adding Sub Category Table to Product",error)


	def add_coloum_order_table(self):
		add_coloumn = ''' ALTER TABLE orders 
						  ADD COLUMN IF NOT EXISTS coupon_code varchar(40),
						  ADD FOREIGN KEY (coupon_code) REFERENCES discounts(coupon_code)'''

		try:
			self.db_connection.cur.execute(add_coloumn)
			self.db_connection.conn.commit()
		except Exception as error:
			print("Error Adding Coupon code to Order",error)

	def drop_subcategory_coloumn(self):
				
		drop_col_obj = Utils(self.db_connection)
		drop_col_obj.drop_column("sub_category", "product_id")


if __name__ == "__main__":
	hostname = 'localhost'
	database = 'shopping'
	username = 'postgres'
	pwd = 'root'
	port_id = 5432

	db = DatabaseConnection(hostname, database, username, pwd, port_id)
	db.connect()

	table_creator = CreateTable(db)

	table_creator.create_user_table()
	table_creator.create_cetegory_table()
	table_creator.create_product_table()
	table_creator.create_shipping_address_table()
	table_creator.create_order_table()
	table_creator.create_order_detail_table()
	table_creator.create_wishlist_table()
	table_creator.create_cart_table()
	table_creator.create_size_table()
	table_creator.create_sales_report_table()
	table_creator.create_discount_table()
	table_creator.create_sub_category_table()
	table_creator.add_size_table()
	table_creator.add_product_table()
	table_creator.add_coloum_order_table()
	# table_creator.drop_subcategory_coloumn()
	
	db.close_connection()
	