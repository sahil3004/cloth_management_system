from database import DatabaseConnection
import psycopg2

class UserRegister:

	def __init__(self,db_connection):
		self.db_connection = db_connection


	def admin_register(self,name, password, email):
		try:
			query = " INSERT INTO users(name, password, email, user_type) VALUES (%s, %s, %s, %s)"
			self.db_connection.cur.execute(query,(name, password, email , True))
			self.db_connection.conn.commit()
			return True
		except Exception as error:
			print("Error during admin registration:" , error)
			return False


	def client_register(self,name, password, email):
		try:
			query = " INSERT INTO users(name, password, email, user_type) VALUES (%s, %s, %s, %s)"
			self.db_connection.cur.execute(query,(name, password, email , False))
			self.db_connection.conn.commit()
			return True
		except Exception as error:
			print("Error during Client registration: , error")
			return False



