from database import DatabaseConnection
from product import ProductManagement
from order import OrderManage
from report import SalesReport
from account_setting import AccountSettings
from category_manage import CategoryManagement
from sub_category import SubCategory
from discount import Discount
import psycopg2


class AdminDashboard:
	def __init__(self,db_connection):
		self.db_connection = db_connection

	def admin_menu(self, user_id):
		while True:
			print("\n Admin Menu: ")
			print("\n1. 📦Product Management")
			print("2. 🛒Order Management")
			print("3. 📁Category Management")
			print("4. 📂Sub-Category Management")
			print("5. 📊Sales and Reports")
			print("6. 💸Discounts Management")
			print("7. ⚙️Account Settings")
			print("8. 🚪Logout")

			choice = input("\nPlease select an option (1-8) : ")
			if choice not in ['1','2','3','4','5','6','7','8']:
				print("Invalid input please try again..")
				continue

			if choice == '1':
				product_obj = ProductManagement(self.db_connection)
				product_obj.product_management()

			elif choice == '2':
				order_obj = OrderManage(self.db_connection)
				order_obj.order_management()

			elif choice == '3':
				category_obj = CategoryManagement(self.db_connection)
				category_obj.category_manage()

			elif choice == '4':
				sub_categegory_obj = SubCategory(self.db_connection)
				sub_categegory_obj.sub_category_menu()

			elif choice == '5':
				report_obj = SalesReport(self.db_connection)
				report_obj.report_display()

			elif choice == '6':
				discount_obj = Discount(self.db_connection)
				discount_obj.discount_menu()	

			elif choice == '7':
				ac_setting = AccountSettings(self.db_connection,user_id)
				ac_setting.account_setting_menu()

			elif choice == '8':
				print("Exiting....")
				break
